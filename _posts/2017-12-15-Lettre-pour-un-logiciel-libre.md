---
layout: post
title: Lettre pour un logiciel libre
subtitle: ""
date: 2017-12-15 15:03:00 +0200
vignette: null
intro: "Billet d'humeur sous forme de lettre ouverte, en réaction au trop entendu « Je ne comprends pas pourquoi il y a encore des gens qui utilisent des logiciels privateurs »"
nom_blog: le blog
permalink: /lettre-pour-un-logiciel-libre/
---

*Note: Ceci est un billet d'humeur en réaction au trop entendu « Je ne comprends pas pourquoi il y a encore des gens qui utilisent des logiciels privateurs »*

Cher logiciel libre,

Je sais que tu ne veux que mon bien.

Je sais que tu veux me protéger des logiciels privateurs qui m'enferment, qui profitent de mes données et de ce que je crée pour mieux me garder chez eux. Je commence à les connaître, tout ces éditeur·trice·s de logiciels qui ne cherchent qu'à garder mon attention un peu plus longtemps, à me rendre un peu plus frustrée pour que je continue à jouer à leurs jeux ou utiliser leur réseau social. Tout ça pour profiter des nouvelles données que je leur servirai, pour mieux les revendre à leurs partenaires publicitaires qui les rendent multimillionnaires.

Ils profitent de ce qu'ils savent de moi et du fonctionnement de mon cerveau pour me rendre triste mais pas trop, heureuse mais pas trop, frustrée mais pas trop, pour que je continue à rafraîchir leurs pages, comme une zombie à la recherche de je ne sais quelle info intéressante. C'est vrai que ça fait longtemps que des infos intéressantes, il n'y en a plus beaucoup…

Je sais aussi que tu milites passionnément. Depuis longtemps, tu te défends et fait parler de toi, réexpliquant patiemment la différence entre toi et les logiciels privateurs, ce que sont les GAFAM, et pourquoi il faut arrêter de les utiliser et les répandre.

Et face à ça, pourtant, souvent, on te répète les mêmes phrases :

"Tu es trop compliqué"
"Oui mais tu es un peu moche quand même !"
"Non mais moi j'y comprends rien, je suis pas doué·e en informatique, tu sais…"

Ça rend fou, je sais. Moi je ne milite pas beaucoup, mais je suis dépitée à chaque fois qu'on m'avance ces arguments lorsque je parle de toi.

Et pourtant…

J'ai beau être convaincue de ton importance, je ne suis pas capable de me passer des logiciels privateurs. J'essaie pourtant, doucement. J'utilise framapad plutôt que GoogleDoc, j'ai rejoins Mastodon car Twitter me rend malheureuse, je crée d'autres moyens de communication que Facebook avec ma famille et mes amis pour pouvoir en partir en gardant contact.

Mais je suis toujours sous MacOS.
J'utilise Sketch pour le design d'interfaces.
Je vais voir plein de vidéos sur Youtube.
J'utilise Slack pour garder contact.
Mon portable est sous Android.
J'utilise Google Maps pour me déplacer en voiture.

Je sais pourtant, qu'ils ne me veulent pas du bien. Mais tu sais, le logiciel libre, parfois ça fait peur. Tu me fais peur. Parce que lorsque je veux poser une question et que je ne sais pas où la poser, je me sens perdue. Lorsque je veux installer un logiciel et que je ne trouve pas le bouton pour télécharger, je me sens mal. Et quand on me parle de ligne de commande, je suis angoissée. J'ai peur de faire une bêtise. Ça fait peur la ligne de commande, tu sais. Pourtant j'ai essayé, mais je ne sais pas bien ce que je fais et où je suis quand je l'utilise. Alors j'ai arrêté. Pour le moment.

Alors cher logiciel libre, lorsque tu sera fâché contre moi car, même si tu existes, je préfère utiliser un logiciel privateur, s'il te plaît, souviens-toi que j'ai essayé, que j'essaie encore, mais que je vais à mon rythme car je suis humaine, et que parfois, j'échoue.
Souviens-toi qu'il me faut du courage pour ne pas céder aux sirènes de la facilité que les logiciels privateurs me chantent. Ils ont beau être dangereux, ils sont gentils avec moi, et même si je sais qu'ils jouent un rôle, parfois il est plus facile d'oublier ce danger que d'y résister. Car eux me guident, et grâce à eux je ne me sens pas perdue, je ne me sens pas faible, je me sens confiante car j'ai l'impression de maitriser ce que je fais. Je sais que c'est pour de faux et qu'ils tirent les ficelles. Mais au moins, lorsque je les utilise, ils ne me font pas peur.

Ce que je te propose, cher logiciel, c'est que plutôt que de te fâcher contre moi, nous essayons, ensemble, de te rendre moins angoissant, pour ceux qui comme moi voudraient faire mieux. Ça ne sera pas facile, car ce n'est pas juste une histoire de maquillage. Il faudra t'apprendre à t'adresser à moi pour que je te comprenne simplement. Et t'apprendre à te présenter pour que je comprenne ce que nous pouvons faire ensemble. Je sais bien que jusqu'à présent, personne ne t'as montré comment faire.

Mais si tu me tends la main et que tu m'aides à te faire confiance, je suis prête à faire le chemin avec toi.

A bientôt.

Maïtané
