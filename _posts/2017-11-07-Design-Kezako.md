---
layout: post
title: Design, Kézako ?
subtitle: ""
date: 2017-11-07 16:30:00 +0200
vignette: null
intro: "Je cherche depuis longtemps la meilleure façon de me présenter lorsqu'on me pose LA question: «Et toi, qu'est ce que tu fais ?».
Lorsque j'ai envie de faire simple, je réponds simplement «Je suis designer», parfois je précise «… d'interfaces» et j'en reste là."
nom_blog: le blog
permalink: /design-kezako/
---

Cet article est le premier d'une série de "… Kézako" qui visent à (ré)expliquer certains mots ou concepts, pour que vous sachiez bien ce que j'ai en tête lorsque je les utilise. Pour démarrer, on va parler de …*design !* \o/

Je cherche depuis longtemps la meilleure façon de me présenter lorsqu'on me pose LA question: "Et toi, qu'est ce que tu fais ?".
Lorsque j'ai envie de faire simple, je réponds simplement "Je suis designer", parfois je précise "… d'interfaces" et j'en reste là. Mais comme la question s'est aussi posée pour ce portfolio et que j'ai passé pas mal de temps à faire jouer les mots ensemble pour trouver la façon de me définir qui me conviendrait, je me suis dit que ça valait le coup de le formuler plus longuement sous forme d'article, plutôt que de laisser chacun·e se faire aléatoirement une idée de ce à quoi j'occupe mes journées ;)

Alors, quand je dis que je suis Designer, ça veut dire quoi ?

# La rencontre de *dessin* et *dessein*

Le mot design que l'on utilise en France est un mot d'origine anglaise (lui-même inspiré du vieux français). Dans la langue de Shakespeare, il signifie à la fois *dessiner* et *concevoir avec dessein*. Si en France nous avons largement tendance à omettre la partie dédiée à la conception, c'est pourtant à la jonction entre *dessin* et *dessein* qu'apparaissent les designers, et il est primordial que l'aspect *concevoir avec dessein* ne passe pas aux oubliettes ! Voyons cela plus précisément :

## Un peu de dessin…

Bon pour le coté *dessin* c'est facile: Il s'agit de représenter des objets, personnages, paysages, concepts… ou n'importe quoi d'autre avec un instrument qui peut être un crayon, une plume, un pinceau, un logiciel professionnel de graphisme, Paint, des nouilles trop cuites… L'important c'est qu'à la fin on puisse visualiser plus ou moins ce que l'on veut dire/ notre concept, même si ce n'est qu'un schéma qui semblera gribouillé par notre chat :)

## …et surtout le dessein !

Il est important d'appuyer sur cet aspect de la définition : Les designers dessinent *avec dessein*, afin de réaliser un produit qui desserve leur but. Et c'est là où se trouve la différence majeure avec l'artiste:
- Un·e artiste cherche à représenter une vision (souvent la sienne)
- Un·e designer cherche à *concevoir pour des utilisateurs*

Le dessein d'un·e designer, ce n'est pas de réaliser quelquechose qui représente un point de vue personnel, mais de servir le besoin d'une ou plusieurs personnes, avec ce qu'iel est en train de concevoir. Si vous ne devez retenir qu'une chose, retenez cela: les designers sont là pour servir, au mieux, les besoins de leurs utilisateur·ice·s: *Il n'y a pas de design sans utilisateur·ice·s !*

# Un peu d'empathie pour votre Design ?

Mais comment savoir ce qui est vraiment utile aux personnes ? Comment, alors que nous sommes nous aussi humains et tenté·e·s par tout les produits absolument nécessaires vendus par tous ces magasins d'objets mignons (Oh un monopoly Pokémon !), sommes-nous censé·e·s savoir mieux qu'elleux ce dont chacun·e a réellement besoin ?

C'est le moment de faire place à la *Recherche Utilisateur·ice* !

La phase de recherche utilisateur·ice est souvent reléguée à l'UX Designer (si il y en a un·e !) qui sera LA personne chargée d'informer l'équipe des besoins auxquels il faudra répondre. Mais peu importe qui s'en charge finalement, l'important sera qu'elle fasse preuve d'empathie afin de se mettre à la place de l'utilisateur·ice à laquelle iel veut rendre service, et non pas en supposant ce qui lui est nécessaire. Pour cela, il existe des tas de méthodes élaborées notamment par les UX Designers, qui sont depuis longtemps confronté·e·s aux biais impliquant que l'on pense mieux savoir que l'autre ce dont iel a besoin. Je l'approfondirai dans un prochain article dédié à l'UX Design :)

Cette phase de recherche utilisateur, permettant de mieux comprendre d'où viennent les personnes, ce qu'elles recherchent et ce qu'elles savent, est primordiale dans la réalisation d'un bon design. Trop souvent, les designers sont considéré·e·s comme des expert·e·s qui "connaissent leur métier" et qui de ce fait n'ont pas besoin de données supplémentaires pour concevoir un produit qu'un cahier des charges initial… Et pourtant nous avons besoin de connaitre les utilisateur·ice·s pour pouvoir concevoir pour elleux. Cet aspect est primordial et pourtant rapidement oublié pour des "raisons de coûts", mais c'est privilégier une réalisation à court terme pour se rendre compte quelques temps plus tard qu'il faut tout redémarrer à zéro car on a conçu un produit à coté de la plaque !

# Les utilisateurs, c'est bon, mangez-en !

L'essence de notre métier de designer est donc là: *Concevoir pour des personnes, afin de leur rendre la vie plus belle !*

Cependant très rapidement, les designers se heurtent à des problématiques qui peuvent émousser cet engagement. Qui décide de ce qui est utile ? De ce qui améliore la vie ? Comment savoir si votre voisin·e d'en face n'a pas précisément besoin de ce superbe parapluie-détecteur-d'humidité-connecté ou de ce nouveau produit nettoyant qui permettra de mieux protéger vos couverts et évitera qu'ils se ternissent après votre prochain repas de famille ?

Il nous faut tout simplement nous remettre en permanence en question face aux informations que nous récoltons, et conserver sans cesse une posture privilégiant un design éthique. L'éthique est une composante indissociable du métier de designer, car les sirènes du marketing et du bullshit sont omniprésentes et demandent un effort constant afin de savoir précisément ce qui est de l'ordre de l'utile et ce qui reste de l'ordre du superflu.

Or, trop souvent dans la conception de nouveaux produits (surtout dans le numérique !), c'est le superflu qui est mis en avant afin de parvenir à grapiller quelques euros supplémentaires ou quelques minutes supplémentaires de votre attention pour vous garder sur tel produit ou telle application. C'est notamment le fer de lance des GAFAM (Goggle, Apple, Facebook, Amazon, Microsoft) qui font tout leur possible pour vous retenir captifs de leurs produits, un peu plus chaque jour, car c'est sur vous et vos données que reposent leur modèle économique. Je ne suis pas sûre qu'on puisse y retrouver le dessein initial qui consistait à servir les intérêts des utilisateur·ice·s… (vous pouvez en lire plus ici: [Sur son lit de mort personne ne se dit j'aurais aimé passer plus de temps sur facebook](https://usbeketrica.com/article/sur-son-lit-de-mort-personne-ne-se-dit-j-aurais-aime-passer-plus-de-temps-sur-facebook) et là avec les [témoignages d'anciens ingénieurs de facebook et google](https://usbeketrica.com/article/temoignages-ingenieurs-facebook-google-economie-attention-addiction-reseaux))


# On résume !

Pour résumer, un Designer c'est une personne dont le métier est de concevoir afin de répondre aux besoins de ses utilisateurs. Pour cela, il dessine afin de représenter ces idées. L'éthique est également une composante indissociable de son métier, car il est important de ne pas créer de nouveaux besoins superflus mais de répondre à ceux, bien réels, qui sont déjà présents. Et enfin, il est important que nous fassions preuve d'empathie afin de savoir se mettre à la place de celleux pour qui nous concevons !

Merci de m'avoir lue et n'hésitez pas à me faire signe sur [twitter](https://twitter.com/maiwann_) si vous avez des idées d'article "…Kézako" !
