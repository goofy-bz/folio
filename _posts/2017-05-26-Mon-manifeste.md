---
layout: post
title: Mon manifeste
subtitle: ""
date: 2017-05-26 18:25:13 +0200
vignette: null
intro: "Je suis designer car j'aime faciliter la vie des gens. Je crois fermement en un monde où chacun·e doit faire sa part pour en améliorer l'ensemble, et la mienne correspond à améliorer l'expérience utilisateur·ice, pour que l'utilisation du numérique cesse d'être une frustration pour redevenir un progrès et un plaisir."
nom_blog: le blog
permalink: /Mon-manifeste/
---
Je suis designer car j'aime faciliter la vie des gens. Je crois fermement en un monde où chacun·e doit faire sa part pour en améliorer l'ensemble, et la mienne correspond à *améliorer l'expérience utilisateur·ice*, pour que l'utilisation du numérique *cesse d'être une frustration pour redevenir un progrès et un plaisir.*

# Design = Dessin & Dessein

Ce qui me plait ? Le *fonctionnel*. Me retrouver face à une problématique alambiquée pour réussir à *concilier contraintes et besoins*, c'est ce que je préfère. Pour le faire au mieux, j'ai un set de compétences dont:

- *La recherche utilisateur* (un des aspects de l'UX Design) qui m'aide à *connaitre les problématiques* rencontrées par les utilisateurs·ices et d'*analyser leurs véritables besoins*. Personae, experience maps, entretiens et tests utilisateurs font partie des méthodes que j'utilise.

- *La conception d'interfaces* qui me permet, en me servant des connaissances acquises grâce aux retours des utilisateurs d'un produit ou des experts métiers pour une application, d'agencer les contenus et construire les différents écrans de façon à ce que *chaque information et action se trouve là où on l'attends.*

-  *Le design d'interfaces*, qui correspond simultanément aux aspects graphiques et ergonomiques. L'objectif principal d'un bon design est de *rendre l'interface utilisable,* ce qui ne peut pas se faire sans être attentif à l'ergonomie, en tenant notamment compte des différentes lois de psychologie cognitive. Le graphisme est là pour servir cet aspect en apportant *cohérence et lisibilité* à l'ensemble des éléments composant l'interface.

# Collaboration

Le numérique et le web sont des secteurs portés par des passionné·e·s, ce qui amène les néophytes à devoir appréhender une grande quantité d'informations peu accessibles au premier abord. J'aime *accompagner* les personnes afin de leur donner les outils pour qu'elles comprennent quelles sont leurs possibilités, leur permettre d'être *autonomes* vis à vis du numérique et les aider à *concrétiser leurs idées.* Cela passe par du temps dédié à la discussion, à du conseil, voire à un temps de co-création pour s'approprier l'intention initiale si cela s'avère judicieux.

# Partage de connaissances

L'origine de mes connaissances est hétérogène car j'ai souvent appris à la faveur de rencontres inopinées, de personnes bienveillantes et aimant partager leurs savoirs. J'aime *transmettre et documenter* ce que je sais, c'est pourquoi la nouvelle version de ce site va bientôt s'agrémenter d'une partie Blog et d'une partie Veille.

Au-delà de cela, j'ai une vraie envie de *partager mes connaissances* avec des personnes curieuses, en *intervenant dans des écoles de graphisme ou d'informatique,* des entreprises, ou encore des start-up, pour parler de *conception centrée utilisateur*, d'UX Design, de Responsive Design, d'ergonomie, ou encore du travail de façon plus globale…

# Expérimenter

Je suis à la recherche de *nouvelles façons de travailler et collaborer.* Associer, essayer, conclure, itérer, pour avancer de façon plus fluide *en créant de la valeur.*

Coté design, plutôt que faire des maquettes pixel perfect, je travaille *en agilité,* via des itérations rapides, en utilisant *des méthodes créatives pour améliorer les cycles de développement.*

Sinon, j'apprends à utiliser git et github pour faciliter mes échanges avec les développeurs·euses et intégrateurs·trices tout en travaillant en open source.

J'ai aussi participé à un [WalkingDev](http://walkingdev.fr/) EventStorming qui me semble correspondre à pas mal de problématiques d'incompréhension (inter développeur·se·s, designers et utilisateur·ice·s) et que j'ai vraiment envie de creuser !

# Éco-conception

J'aime *concevoir sans fioritures ni ornements démesurés.* Imaginer une solution simple pour répondre à un problème, en l'épurant au maximum pour faciliter sa mise en place. Je compose donc des interfaces minimalistes afin de réaliser des sites et applications *rapides, simples, clairs et efficaces.*

# Mobile first & Responsive design

Réaliser des sites adaptés à n'importe quelle taille d'écran est une de mes contraintes préférées. Je choisis donc prioritairement de fonctionner en *mobile first,* c'est à dire de *concevoir d'abord pour le mobile* et ensuite pour les tailles d'écran plus larges, afin que l'expérience sur smartphone ne soit pas "dégradée" par rapport à celle d'un grand écran, mais qu'elle soit *adaptée quelque soit le support.*

# Conception inclusive & accessible

Que ce soit un site ou une application métier, j'aime *concevoir pour des gens différents.* L'accessibilité et le design inclusif, qui en est la prolongation, sont des principes qui me tiennent à coeur. J'essaye donc de réaliser *des interfaces adaptées à tou·te·s,* en gardant à l'esprit les contraintes de chacun·e·s.

# Bien-être & Plaisir dans le travail

Le numérique est une passion, ce qui ne m'empêche pas par moments de partir faire un tour sans aucun appareil électronique sur moi.
Je cherche à construire *une façon de travailler qui allie plaisir et bien-être,* afin que la qualité de mon travail ne se fasse jamais *au détriment de ma santé ou mon éthique.*

# Ergonomie & Santé au travail

Comme j'aime beaucoup apprendre, j'ai rejoint la formation du Conservatoire Nationale des Arts et Métiers de Toulouse *en ergonomie pour y apprendre de nouvelles méthodes et acquérir des connaissances (en psychologie cognitive notamment).* A ma grande surprise, j'y ai découvert tout un pan de la discipline qui correspond à l'observation de la santé et du bien-être des travailleur·se·s à travers les contraintes de performance, organisationnelles et matérielles d'une entreprise. J'en apprends donc beaucoup sur *l'évolution du travail* en France et des impacts qu'il peut avoir sur chacun·e.

Je découvre également *des méthodes d'analyse de l'activité* (grilles d'observations, halo-confrontations, chroniques d'activité…), qui viennent enrichir ma boite à outils d'UX Designer.

# Wishlist

Pour conclure, j'ai élaboré une liste des types de projets qui résonnent avec ceux que j'aimerai réaliser.
Vous l'aurez compris, si votre projet porte des valeurs telles que *l'accessibilité, l'open source, la bienveillance, l'inclusivité, l'éthique* (liste non exhaustive), j'aimerai discuter avec vous de ce que je peux vous apporter.
J'ai aussi des thèmes de prédilections qui sont *l'environnement, l'enfance, l'éducation, la solidarité, le service public, l'engagement citoyen et l'économie sociale et solidaire.*

N'hésitez pas à [me contacter](http://www.maiwann.net/me_contacter/) si vous avez des questions, des remarques, ou si vous avez envie de discuter =)
