---
layout: post
title: Partager pour mieux inspirer
subtitle: ""
date: 2017-12-19 16:32:00 +0200
vignette: null
intro: "Je suis curieuse. Je n'y peux rien, même si on dit que c'est un vilain défaut, c'est une partie insatiable de ma personnalité : j'adore découvrir et apprendre de nouvelles choses. Ce qui me transcende lorsque j'apprends, c'est de découvrir un petit quelque chose qui élargira ma vision du monde. Alors je cherche, je lis, j'accumule des connaissances, je découvre de nouvelles façons de penser et parfois, j'ai un déclic, dû à une phrase, un mot, une tournure qui rend évident quelque chose, et mon univers s'agrandit."
nom_blog: le blog
permalink: /partager-pour-mieux-inspirer/
---

Je suis curieuse. Je n'y peux rien, même si on dit que c'est un vilain défaut, c'est une partie insatiable de ma personnalité : j'adore découvrir et apprendre de nouvelles choses. Ce qui me transcende lorsque j'apprends, c'est de découvrir un *petit quelque chose qui élargira ma vision du monde.* Alors je cherche, je lis, j'accumule des connaissances, je découvre de nouvelles façons de penser et parfois, j'ai un déclic, dû à une phrase, un mot, une tournure qui rend évident quelque chose, et mon univers s'agrandit.

Mais pour que cette petite étincelle arrive, pour que je puisse avoir des étoiles dans les yeux en comprenant un nouveau concept, j'ai besoin d'être inspirée.


# L'inspiration n'arrive pas toute seule

Heureusement, je suis entourée de personnes qui aiment partager leurs connaissances et leurs découvertes.

Iels racontent [un choix de vie qu'iels ont fait](https://www.julienpradet.fr/autres/changement-de-rythme/), [ce qu'iels vivent au quotidien](http://libelilou.github.io/2017/10/25/ehpad.html), [le projet qui suit leurs valeurs qu'iels ont mis en route](https://medium.com/mercicookie/cest-quoi-ces-cookies-dont-on-parle-tout-le-temps-a-la-fin-15726b9cdaf5), [la façon dont iels cuisinent](https://medium.com/@bndctlmbrt/cuisine-cr%C3%A9ative-et-collaborative-38af747b7cea), [comment iels ont vécu un évènement](https://medium.com/@julia.barbelane/comment-mon-badge-pour-sudweb-a-atterrit-au-milieu-des-mes-photos-de-famille-146283e2e219), [quels choix les ont amené·e·s à monter leur boite de telle façon](https://dtc-innovation.org/writings/2017/une-association), [de quelle façon iels travaillent au jour le jour](https://www.youtube.com/watch?v=DgAs2ifekNo), [quand ça a été compliqué pour elleux](https://medium.com/burnout-rallumons-la-flamme/le-burnout-et-moi-5c8f9cc4d0d9), [des choses rigolotes qu'iels ont envie de faire pour elleux ou pour d'autres](http://tut-tuuut.github.io/2016/08/15/projet-creatif-secret-ohelina.html)…

La lecture de chacun de ces textes et vidéos m'a inspirée. J'y ai trouvé du courage pour me diriger vers mes objectifs personnels et professionels (qui sont intimement liés), en sachant que d'autres étaient déjà passé·e·s par là, qu'iels sont autant d'exemples de ce qui est possible que de personnes bien réelles auxquelles je pourrais écrire si j'en ressentais le besoin.

C'est grâce à ces personnes que j'arrive à être positive et enthousiaste, que j'apprends des choses dans de nouveaux domaines auxquels il y a quelques semaines je ne connaissais strictement rien, bref que je progresse en tant que professionnelle mais aussi en tant qu'individu.


# De la nécessité de documenter

Pourtant je n'ai pas toujours été entourée de tous ces textes et vidéos inspirantes. Et lorsque j'ai été vraiment perdue, à ne pas savoir quoi faire ni vers où me diriger, ce n'est que grâce à ces personnes que je n'ai pas sombré dans la détresse.

Voilà pourquoi, en vérité, cet article est un appel.

J'ai besoin de vous.

J'ai besoin que vous me racontiez ce que vous aimez faire ou ce que vous voudriez réaliser.

Ce qui fait qu'au fond de vos tripes, vous avez été heureux·se de faire quelque chose.

Ou simplement que vous parliez de ce que vous maîtrisez et que vous avez envie de partager.

Que vous me parliez de vous.

Car, parfois, c'est en vous lisant que je me comprends mieux. Et c'est grâce à cela que je me construis, et que je vois vers où je vais.

Alors je vous en prie, prenez le temps, régulièrement, d'écrire, de prendre en photo, de filmer, *afin qu'il soit possible de retracer ce que vous réalisez.* Car quelque part, il y a forcément des personnes qui sont interessées par vos connaissances, pour apprendre elles aussi de vos erreurs et de vos expériences, pour avancer et progresser. Il y aura toujours une personne qui découvrira votre domaine et sera avide de connaissances s'adressant à elle, sans prétention. Simplement pour savoir ce que vous avez vécu, ce que vous savez, ce que vous faites.


# « J'ai rien à dire d'intéressant »

Évidemment, dès qu'on parle de diffuser ses connaissances, notre syndrôme de l'imposteur·se arrive à toute vitesse pour nous faire dire « De toute façon, moi, j'ai rien de spécial à raconter ».

Soyons clair·e·s tout de suite : *c'est faux*

Car même si vous racontiez quelque chose qui avait déjà été dit 1000 fois, c'est la façon dont vous le direz qui fait une différence. Et pour avoir entendu une même idée des dizaines de fois avant qu'une formulation, une métaphore, une image ne s'impose à moi comme une évidence, il aura fallu un certain nombres de personnes qui se seront répétées.

De plus, nous sommes malheureusement bombardé·e·s d'informations inintéressantes nous répétant de grands préceptes qui privilégient pour la plupart d'avantage l'intérêt financier et productiviste que la possibilité de s'écouter et de faire les choses pour son bien-être. Il est donc, à mon sens, très important de semer les alternatives, les expériences individuelles, pour qu'elles prennent de l'ampleur et se fassent davantage entendre.

Vous avez des choses à dire. Bien plus que vous le pensez et même si c'est imparfait, cela sera toujours mieux que rien.

(Et si vous doutez toujours de vous, courez lire le billet de Marie Guillaumet sur l'expertise [On ne naît pas expert·e, on le devient](https://www.24joursdeweb.fr/2017/on-ne-nait-pas-expert-e-on-le-devient/))

# En résumé

Documentez, écrivez, partagez, même si c'est imparfait, même si vous voudriez faire plus, faites-le un peu plutôt que pas du tout, afin que nos horizons puissent s'agrandir et nos connaissances se multiplier <3

J'ai hâte de vous lire !


~~ Merci à Marie pour la relecture 💜
